var Mongoose = require('mongoose')
var Schema = Mongoose.Schema
var userschema = new 
Schema(
    {
        email:{type:String,unique:true,required:true},
        password:{type:String,required:true},
        verified:{type:Boolean,default:false},
        sessionToken:{type:String},
        createdat:{type:Date, default:new Date().getTime()},
        verificationtoken:{
            type: Number,
            default: new Date().getTime()
        }
    }
)

module.exports = Mongoose.model('user', userschema);