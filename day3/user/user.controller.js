var UserService = require('./user.service')

exports.verifyuser = (req, res)=>{
    var token = req.query.token;
    UserService.verify(token)
    .on('ERROR',()=>{
        message:"Some error occured!"
    })
    .on('VERIFIED',()=>{
        console.log('verified')
        res.send({
            message:"You are verified"
        })
        
    })
    .on('EXPIRED',()=>{
        res.send({
            error:"Your session has expired. Please login again!"
        })
    })
}