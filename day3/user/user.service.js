var Jwt = require('jsonwebtoken')
var UserModel = require('./user.model.js')
var EventEmitter = require('events')
var CommonService = require('./common.service')

exports.findUser = (data)=>{
    return new Promise((resolve, reject)=>{
        if(!data){
            reject()
        }
        else{
            
        }
    })
}

exports.createUser = (data)=>{
    let emitter = new EventEmitter()
    if(!data){
        emitter.emit('ERROR NO DATA')
    }
    else{
        var userdata = new UserModel(data);
        userdata.save((err, newUser)=>{
            if(!err){
                console.log('User Created Successfully!', newUser)
                emitter.emit('SUCCESS', newUser)
                var verifylink =  CommonService.createLink(newUser)
                CommonService.sendVerifyMail(newUser)
                console.log('Verification link: ', verifylink)
            }
            else{
                console.log(err)
                emitter.emit('ERROR',err)
            }
        })
    }

return emitter
}

exports.verify = (token)=>{
    let emitter = new EventEmitter()
    var currenttime = new Date().getTime()
    console.log(currenttime, token)
    var diff = currenttime - token
    if (diff < 86400000){
        console.log('inside if')
        UserModel.findOneAndUpdate({verificationtoken:token},{$set:{verified:true}},(err, data)=>{
            if(err){
                emitter.emit('ERROR')
            }
            else{
                emitter.emit('VERIFIED')
            }
        })
    }
    else{
        emitter.emit('EXPIRED')
    }

    return emitter
    
}