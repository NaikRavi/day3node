var UserService = require('../user/user.service')
exports.createNewUser = (req, res)=>{
    userData = req.body;
    console.log(userData)
    if(userData.email && userData.password){
        UserService.createUser(userData)
        .on('ERROR',(err)=>{
            res.send(err)
        })
        .on('SUCCESS',(success)=>{
            res.send({
                message:'User created successfully'
            })
        })

    }
    else{
        res.send({
            error:"Please send email and password"
        })
    }
}