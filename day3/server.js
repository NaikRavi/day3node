var Express = require('express')
const port = process.env.PORT || 5000;
var Mongoose = require('mongoose');
var BodyParser = require('body-parser')
const mongo_url = "mongodb://localhost:27017/bdcmay";
var app = Express();
var UserService = require('./user/user.service')
const routes = require('./routes.js');
app.use(BodyParser.json())

Mongoose.connect(mongo_url, (err)=>{
    if(err){
        console.log('Connection Error!', err)
    }
    else{
        console.log('Database Connection Successfull!')
        // var data = {email:'ravi.naik2', password:'1234'}
        // UserService.createUser(data)
        // .on('ERROR',(error)=>{
        //     console.log('Error in creating user,server',error)
        // })
        // .on('SUCCESS',(success)=>{
        //     console.log('Successful in serverjs',success)
        // })
        app.use(routes)
    }
})

app.listen(port, ()=>{
    console.log('Server is Running!')
})