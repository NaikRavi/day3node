var AdminController = require('./admin.controller')

var Express = require('express')
var router = Express.Router();

router.post('/signup', AdminController.createNewUser)
// router.post('/login', AdminController.login)

module.exports = router;

